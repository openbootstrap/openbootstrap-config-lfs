#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

mkdir build
cd build

../configure \
    --prefix="$ROOT/tools" \
    --host="$TARGET" \
    --build="$(../scripts/config.guess)" \
    --enable-kernel="$LINUX_VERSION" \
    --with-headers="$ROOT/tools/include"
make -j"$THREADS"
make install
