#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

mkdir build_libstdc++
cd build_libstdc++
../libstdc++-v3/configure \
    --host=$TARGET \
    --prefix=$ROOT/tools \
    --disable-multilib \
    --disable-nls \
    --disable-libstdcxx-threads \
    --disable-libstdcxx-pch \
    --with-gxx-include-dir=$ROOT/tools/$TARGET/include/c++/$GCC_VERSION
make -j$THREADS
make install
