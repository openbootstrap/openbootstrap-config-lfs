#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# Change dynamic linker to binutils in /tools, remove /usr/include from search path
for file in $("gcc/config/linux.h" "gcc/config/i386/linux.h" "gcc/config/i386/linux64.h")
do
    cp -u "$file" "$file.orig"
    sed -e "s@/lib\(64\)\?\(32\)\?/ld@$ROOT/tools&@g" -e "s@/usr@$ROOT/tools@g" "$file.orig" > "$file"
    printf "#undef STANDARD_STARTFILE_PREFIX_1\n#undef STANDARD_STARTFILE_PREFIX_2\n#define STANDARD_STARTFILE_PREFIX_1 \"%s/tools/lib/\"\n#define STANDARD_STARTFILE_PREFIX_2 \"\"\n" "$ROOT" >> "$file"
    touch "$file.orig"
done

# Change library directory from lib64 to lib if building x86_64 since this is not a multilib system
case $ARCH in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
 ;;
esac

mkdir build_gcc
cd build_gcc

../configure \
    --target="$TARGET" \
    --prefix="$ROOT/tools" \
    --with-glibc-version=2.11 \
    --with-sysroot="$ROOT" \
    --with-newlib \
    --without-headers \
    --with-local-prefix="$ROOT/tools" \
    --with-native-system-header-dir="$ROOT/tools/include" \
    --disable-nls \
    --disable-shared \
    --disable-multilib \
    --disable-decimal-float \
    --disable-threads \
    --disable-libatomic \
    --disable-libgomp \
    --disable-libquadmath \
    --disable-libssp \
    --disable-libvtv \
    --disable-libstdcxx \
    --enable-languages=c,c++
make -j"$THREADS"
make install
