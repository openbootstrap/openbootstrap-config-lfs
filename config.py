#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# The architecture of your operating system. Set this to the architecture of your computer,
# since LFS does not support cross compilation.
arch = "x86_64"

# This is the vendor section of the target triplet (the pc in x86_64-pc-linux-gnu). Set this to the name of your operating system.
vendor = "lfs"

# The number of threads to use to compile. If set to 0 the number is automatically computed based on your CPU core count.
# If you experience segfaults set this number manually to something lower than your CPU core count.
# If you continue to experience segfaults set this number to 1.
threads = 6

# Which version of software to use. If you want your distro to be more stable like Debian, then use older versions.
# If you want your distro to be bleeding edge like Arch, keep the default versions since they track the newest
# releases of the packages.
software_versions = {
    "binutils": "2.34",
    "gcc": "9.3.0",
    "linux": "5.4.49",
    "glibc": "2.31",
    "m4": "1.4.18",
    "ncurses": "6.2",
    "dash": "0.5.11",
    "coreutils": "8.32",
    "diffutils": "3.7",
    "file": "FILE5_39",
    "findutils": "4.7.0",
    "gawk": "5.1.0",
    "grep": "3.4",
    "gzip": "1.10",
    "make": "4.3",
    "patch": "2.7.6",
    "sed": "4.8",
    "tar": "1.32",
    "xz-utils": "5.2.5",
    "bison": "3.6.3",
    "gettext": "0.20.2",
    "perl": "5.30.3",
    "python": "3.8.3",
    "texinfo": "6.7",
    "util-linux": "2.35.2",
    "tcl": "8.6.10",
    "expect": "5.45.4",
    "dejagnu": "1.6.2"
}

# Which compression format to use for each package. XZ is preferred since it saves bandwidth but sometimes
# a certain version of a package isn't available in XZ format so we use GZ instead. The tarfile backend used
# to unarchive and decompress source tarballs can handle .tar.gz, .tar.bz2, and .tar.xz files.
software_formats = {
    "binutils": "xz",
    "gcc": "xz",
    "linux": "xz",
    "glibc": "xz",
    "m4": "xz",
    "ncurses": "gz",
    "dash": "gz",
    "coreutils": "xz",
    "diffutils": "xz",
    "file": "gz",
    "findutils": "xz",
    "gawk": "xz",
    "grep": "xz",
    "gzip": "xz",
    "make": "gz",
    "patch": "xz",
    "sed": "xz",
    "tar": "xz",
    "xz-utils": "xz",
    "bison": "xz",
    "gettext": "xz",
    "perl": "gz",
    "python": "xz",
    "texinfo": "xz",
    "util-linux": "xz",
    "tcl": "gz",
    "expect": "gz",
    "dejagnu": "gz"
}

#
# URLs to use to download source files. Change this if one of the file servers goes down.
#

gnu_url = "https://ftp.gnu.org/gnu"
kernel_org_url = "https://cdn.kernel.org/pub"
cpan_url = "https://www.cpan.org"
python_url = "https://python.org/ftp/python"
sourceforge_url = "https://sourceforge.net"

def gnu(name):
    return f"{gnu_url}/{name}/{name}-{software_versions[name]}.tar.{software_formats[name]}"

software_urls = {
    "binutils": gnu("binutils"),
    "gcc": f"{gnu_url}/gcc/gcc-{software_versions['gcc']}/gcc-{software_versions['gcc']}.tar.{software_formats['gcc']}",
    "linux": f"{kernel_org_url}/linux/kernel/v{software_versions['linux'].split('.')[0]}.x/linux-{software_versions['linux']}.tar.{software_formats['linux']}",
    "glibc": gnu("glibc"),
    "m4": gnu("m4"),
    "ncurses": gnu("ncurses"),
    "dash": f"http://gondor.apana.org.au/~herbert/dash/files/dash-{software_versions['dash']}.tar.{software_formats['dash']}",
    "coreutils": gnu("coreutils"),
    "diffutils": gnu("diffutils"),
    "file": f"https://github.com/file/file/archive/{software_versions['file']}.tar.{software_formats['file']}",
    "findutils": gnu("findutils"),
    "gawk": gnu("gawk"),
    "grep": gnu("grep"),
    "gzip": gnu("gzip"),
    "make": gnu("make"),
    "patch": gnu("patch"),
    "sed": gnu("sed"),
    "tar": gnu("tar"),
    "xz-utils": f"{sourceforge_url}/projects/lzmautils/files/xz-{software_versions['xz-utils']}.tar.{software_formats['xz-utils']}/download",
    "bison": gnu("bison"),
    "gettext": gnu("gettext"),
    "perl": f"{cpan_url}/src/5.0/perl-{software_versions['perl']}.tar.{software_formats['perl']}",
    "python": f"{python_url}/{software_versions['python']}/Python-{software_versions['python']}.tar.{software_formats['python']}",
    "texinfo": gnu("texinfo"),
    "util-linux": f"{kernel_org_url}/linux/utils/util-linux/v{software_versions['util-linux'].split('.')[0]}.{software_versions['util-linux'].split('.')[1]}/util-linux-{software_versions['util-linux']}.tar.{software_formats['util-linux']}",
    "tcl": f"{sourceforge_url}/projects/tcl/files/Tcl/{software_versions['tcl']}/tcl{software_versions['tcl']}-src.tar.{software_formats['tcl']}/download",
    "expect": f"{sourceforge_url}/projects/expect/files/Expect/{software_versions['expect']}/expect{software_versions['expect']}.tar.{software_formats['expect']}/download",
    "dejagnu": gnu("dejagnu")
}

# The order in which to run the compilation scripts. The key is the name of the script (minus .sh)
# and the value is the name of the source directory that the script should be run from.
scripts = {
    "binutils_stage1": "binutils",
    "gcc_stage1": "gcc",
    "linux-headers": "linux",
    "glibc_stage1": "glibc",
    "libstdc++_stage1": "gcc"
}
